import Link from 'next/link';
import Layout from '/components/layout';
import Header from '/components/header';
import styles from '/styles/aboutus.module.scss';
import Nav from '/components/navbar'
import Banner from '/components/banner'
import Footer from '/components/footer'
import Image from "next/image";

export default function About() {
  return (
    <Layout>
      <Header title="About us"/>
      <Nav />
      <Banner 
          imgSrc="/banner2.jpg" 
          imgFit="cover"
          contentPadding="150px"
          contentPosition="end"
          contentWidth="100%"
          minHeight="200px"
          title="NextJS Banner"  
          titleColor="#51a5ff" 
          titleAlignment="center" 
          subtitle="About Us banner"  
          subtitleAlignment="left" 
          description="About Us description" 
          descriptionAlignment="left"
          ctaText="About Us" 
          ctaTextColor="#51a5ff"
          ctaBgColor="white"
          ctaAlignment="left"
          ctaClasses="border-radius-1"
      />
      <div className='container py-5'>
        <div className='d-flex'>
          <h1 className={styles.title}>About Us</h1> 
        </div>
        <div className='col-sm-10'>
          <Image
            src="/dummy.jpg"
            width={500}
            height={300}
            layout="responsive"
            objectFit='cover' 
            quality={80}
            className='my-3'
          />
        </div>
   
        <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h4>
        <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h4>
      </div>
      <Footer />
    </Layout>
  );
}
/*const Hello = () => {
  return ( <div>
    
  </div> );
}
 
export default Hello;*/