import { useState } from 'react';
import Head from 'next/head';
import Nav from '/components/navbar'
import Banner from '/components/banner'
import Footer from '/components/footer'
import Image from "next/image";
import Layout from '/components/layout';
import Header from '/components/header';

function Title({ title }) {
  return <h1>{title ? title : 'Default title'}</h1>;
}

export default function HomePage() {
  const names = ['Lorem', 'Lorem Lorem', 'Lorem Lorem Lorem'];

  const [likes, setLikes] = useState(0);

  function handleClick() {
    setLikes(likes + 1);
  }

  return (
    
    <Layout>
      <Header title="Home"/>
      <Nav />
      <Banner 
          imgSrc="/banner.jpg" 
          imgFit="cover"
          contentPadding="150px"
          contentPosition="center"
          contentWidth="1320px"
          minHeight="600px"
          title="NextJS Banner"  
          titleColor="#154E8A" 
          titleAlignment="center" 
          subtitle="Home banner"  
          subtitleAlignment="center" 
          description="Home description" 
          descriptionAlignment="center"
          ctaText="Home" 
          ctaTextColor="white"
          ctaBgColor="#154E8A"
          ctaAlignment="center"
          ctaClasses="border-radius-1"
      />
      <div className='container py-5'>
        <Title title="Nextjs Test Project" />
        <ul className='pt-3'>
          {names.map((name) => (
            <li key={name}>{name}</li>
          ))}
        </ul>
        <Image
          src="/dummy.jpg"
          width={500}
          height={300}
          objectFit="cover"
        />
        <h4 className='pt-4'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h4>

      </div>
     {/*<button onClick={handleClick}>Like ({likes})</button>*/} 
     <Footer />
    </Layout>
  );
}