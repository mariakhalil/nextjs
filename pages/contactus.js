import Link from 'next/link';
import Layout from '/components/layout';
import Header from '/components/header';
import styles from '/styles/contactus.module.scss';
import Nav from '/components/navbar'
import Banner from '/components/banner'
import Footer from '/components/footer'
import Image from "next/image";

export default function Contactus() {
  return (
    <Layout>
      <Header title="Contact us"/>
      <Nav />
      <Banner 
          imgSrc="/banner1.jpg" 
          imgFit="cover"
          contentPadding="100px"
          contentPosition="start"
          contentWidth="1320px"
          minHeight="500px"
          title="NextJS Banner"  
          titleColor="#053569" 
          titleAlignment="left" 
          subtitle="Contact Us banner"  
          subtitleAlignment="left" 
          description="Contact Us description" 
          descriptionAlignment="left"
          ctaText="Contact Us" 
          ctaTextColor="#154E8A"
          ctaBgColor="white"
          ctaAlignment="left"
          ctaClasses="border-radius-2"
      />
      <div className='container py-5'>
        <div className='d-flex'>
          <h1 className={styles.title}>Contact Us</h1>
        </div>
        <Image
          src="/dummy.jpg"
          width={500}
          height={300}
          layout="responsive"
          className='my-3 image'
        />
        <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h4>
        <h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h4>
      </div>
      <Footer />
    </Layout>
  );
}
/*const Hello = () => {
  return ( <div>
    
  </div> );
}
 
export default Hello;*/