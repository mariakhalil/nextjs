import Link from 'next/link';
import Layout from '/components/layout';
import Header from '/components/header';
import styles from '/styles/comments.module.scss';
import Nav from '/components/navbar'
import Footer from '/components/footer'

export const getStaticPaths = async () => {

    const res = await fetch("https://jsonplaceholder.typicode.com/comments");
    const data = await res.json();

    const paths = data.map(review => {
        return {
            params : { id : review.id.toString() } //array of objects
        }
    })
   
    return{
        paths,
        fallback: false //shows 404 page if id doesnt exist
    }

}

export const getStaticProps = async(context) => {

    const id = context.params.id;
   
    const res = await fetch('https://jsonplaceholder.typicode.com/comments/' +id);
    const data = await res.json();

    return{
        props: { review : data }
    }
}
 
const Details = ({ review }) => {
  return ( 
    <Layout>
        <Header title="Reviews"/>
        <Nav />
        <div className='container py-5 min-vh-100'>
            <div className='col-sm-6'>
                <div className='d-flex'>
                    <h1 className={styles.title}>Review Details</h1>
                </div>
                <h4 className='pt-5'>{review.name}</h4>
                <p>{review.email}</p>
                <p>{review.body}</p>
            </div>
        </div>
        <Footer />
    </Layout>
  );
}
 
export default Details;