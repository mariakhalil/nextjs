import Link from 'next/link';
import Layout from '/components/layout';
import Header from '/components/header';
import styles from '/styles/comments.module.scss';
import Nav from '/components/navbar'
import Banner from '/components/banner'
import Footer from '/components/footer'

export const getStaticProps = async () =>{

  const res = await fetch('https://jsonplaceholder.typicode.com/comments');
  const data = await res.json();

  return{
    props: {reviews:data}
  }

}

const Comments = ({reviews}) => {
  return ( 
    <Layout>
      <Header title="Reviews"/>
      <Nav />
      <Banner 
          imgSrc="" 
          imgFit="cover"
          imgAltText="banner"
          contentPadding="150px"
          contentPosition="end"
          contentWidth="1320px"
          minHeight="600px"
          title="NextJS Banner"  
          titleColor="black" 
          titleAlignment="center" 
          subtitle="Reviews banner"  
          subtitleAlignment="right" 
          description="Reviews description" 
          descriptionAlignment="right"
          ctaText="Reviews" 
          ctaAlignment="right"
      />
      <div className='container py-5'>
        <div className='d-flex'>
          <h1 className={styles.title}>Users Reviews</h1>
        </div>
        <h4 className='pt-5'>All reviews</h4>
        <div>
          {reviews.map(function(review,index){
            return (index <= 4) ? (
              <div>
                <h5 className='pt-3'>Review {index+1}</h5>
                <Link className={styles.review} href={"/comments/" + review.id} key={review.id}>
                <h4>{review.body}</h4>
                </Link>
              </div>
            ):(
              <div></div>
            );
          })}

        </div>
      </div>
      <Footer />
    </Layout>
  );
}
 
export default Comments;