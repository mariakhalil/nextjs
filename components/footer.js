import Link from 'next/link';
import styles from '/styles/footer.module.scss';
const Footer = () => {
    return (
        <div className=  {`${styles.footer}  py-2`}>
          <div className='container d-flex justify-content-between align-items-center'>
            <div className=''>
                <h3 className={styles.text}>© 2022 Nextjs. All rights reserved.</h3>
            </div>
          </div>
        </div>
    );
}
 
export default Footer;