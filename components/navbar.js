import Link from 'next/link';
import styles from '/styles/nav.module.scss';
import Image from "next/image";
const Nav = () => {
    return (
        <nav className=  {`${styles.navbar}  py-2`}>
          <div className='container d-flex justify-content-between align-items-center'>
            <div className=''>
                <Image
                    src="/next_logo.png"
                    width={70}
                    height={70}
                    className='my-3'
                />
                  
                </div>
                <div>
                    <Link className={`${styles.links} px-2`} href="/">Home</Link>
                    <Link className={`${styles.links} px-2`} href="/aboutus">About us</Link>
                    <Link className={`${styles.links} px-2`} href="/contactus">Contact us</Link>
                    <Link className={`${styles.links} px-2`} href="/comments">Reviews</Link>
                </div>
          </div>
        </nav>
    );
}
 
export default Nav;