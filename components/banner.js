import styles from '/styles/banner.module.scss'; 
import Image from "next/image";

export default function Banner({
    imgSrc, imgFit, imgAltText, imgWidth,
    contentPadding, contentPosition, contentWidth, minHeight,
    title,  titleColor, titleAlignment,
    subtitle, subtitleAlignment,
    description, descriptionAlignment,
    ctaText, ctaTextColor, ctaBgColor, ctaAlignment, ctaClasses
    }) {

  return ( 
    <>
        <div class= {`${styles.banner}`} style={{minHeight : ` ${minHeight}`}}>
            <div class={`${styles.media}`}>
                 <Image
                    src={ `${imgSrc}`  ||  "/dummy.jpg" } 
                    alt={`${imgAltText}` || `${title}` }
                    width={700}
                    height={300}
                    objectFit={`${imgFit}` || "cover"}
                    className={`${styles.image}`}
                />
            </div>
            <div class={`${styles.bcontainer}`} style={{maxWidth : `${contentWidth}`}}>
                <div class={`${styles.flex}`}>
                    <div class={`${styles.content}`} style={{padding: ` ${contentPadding}` , justifyContent : `${contentPosition}`}}>
                        <div>
                            <h1 style={{ color: ` ${titleColor}` , textAlign: ` ${titleAlignment}`}}>{ title }</h1>
                            <h2 class={`${styles.paddingtop}`} style={{ textAlign: ` ${subtitleAlignment}`  }}>{ subtitle }</h2>
                            <div class={`${styles.paddingtop}`} style={{ textAlign: ` ${descriptionAlignment}`  }}>{ description }</div>
                            <div style={{textAlign: ` ${ctaAlignment}`}} >
                               <a href="" target="_blank" className={`${styles.cta}  ${ctaClasses}`}
                                style={{background : ` ${ctaBgColor}` , color : ` ${ctaTextColor}` }} >
                                { ctaText }</a>   
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
  );
}